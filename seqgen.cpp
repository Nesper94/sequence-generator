#include <iostream>
#include <random>
#include <vector>
#include <string>
#include "seqgen-config.h"

std::random_device randev;
std::vector<char> bases{'A','C','G','T'};
double A{1.0};
double C{1.0};
double G{1.0};
double T{1.0};

void usage(){
    std::cout <<
        "Usage: " << PROJECT_NAME << " [OPTION]... LENGTH\n" <<
        "Create a random DNA sequence of the specified LENGTH\n\n" <<
        "Options:\n" <<
        "  -A  Proportion of Adenine (default=1)\n" <<
        "  -C  Proportion of Cytosine (default=1)\n" <<
        "  -G  Proportion of Guanine (default=1)\n" <<
        "  -T  Proportion of Thymine (default=1)\n" <<
        "  -h, --help\n" <<
        "      display this help and exit\n" <<
        "  -v, --version\n" <<
        "      output version information and exit\n";
}

int main(int argc, char* argv[]){

	if (argc < 2){
		std::cerr << "Error: Wrong number of arguments\n" <<
    "Try '" << PROJECT_NAME << " --help' for more information.\n";
		return 9;
	};

    // Parse arguments
    for (int i = 1; i < argc; ++i) {
        std::string arg{ argv[i] };

        if (arg == "-h" || arg == "--help"){
            usage();
            return 0;
        } else if (arg == "-v" || arg == "--version"){
            std::cout << PROJECT_NAME << " version " <<
            seqgen_VERSION_MAJOR << "." << seqgen_VERSION_MINOR <<
            "." << seqgen_VERSION_PATCH <<
            "\n\nWritten by Juan Camilo Arboleda Rivera\n";
            return 0;
        } else if (arg == "-A") {
            A = std::stod(argv[++i]);
        } else if (arg == "-C") {
            C = std::stod(argv[++i]);
        } else if (arg == "-G") {
            G = std::stod(argv[++i]);
        } else if (arg == "-T") {
            T = std::stod(argv[++i]);
        }
    }

    std::discrete_distribution<int> distribution {A,C,G,T};
	std::default_random_engine generator(randev());

	int len = std::atoi(argv[argc - 1]);

	for (int i = 0; i < len; ++i){
		std::cout << bases[ distribution(generator) ];
	};
	std::cout << '\n';
	return 0;
}
