# Sequence Generator

Simple random DNA sequence generator.

Usage:

```
seqgen [OPTION]... LENGTH

Options:
  -A  Proportion of Adenine
  -C  Proportion of Cytosine
  -G  Proportion of Guanine
  -T  Proportion of Thymine
```
